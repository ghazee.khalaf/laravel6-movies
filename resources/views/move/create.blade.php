@extends('move.layout')
@section('content')
    <div class="container">
        <!-- -->
        <img src="/images/feature.jpg" alt="MOVIE">
        <form action="{{ route('moveis.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group ">
                <label for="exampleFormControlInput1">Movie name</label>
                <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Movie name">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Movie Catagory</label>
                <select class="form-control" name="category_id">
                    @foreach ($category as $cat)
                        <option value="{{ $cat->id }}" {{ $cat->id == old('category_id') ? 'selected' : '' }}>{{ $cat->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Movie bio</label>
                <textarea class="form-control" name="bio"  rows="2">{{ old('bio') }}</textarea>
            </div>
            <div class="form-group ">
                  <label for="exampleFormControlInput1">Movie Cover photo</label>
                <input type="file" name="image_path" class="form-control">
            </div>
            <div class="form-group ">
                <label for="exampleFormControlInput1">Movie rating</label>
                <input type="number" class="form-control" name="rating" value="{{ old('rating') }}" min=" 1" max="10">
            </div>
            <div class="form-group ">
                <label for="exampleFormControlInput1">Movie actor</label>
                <input type="text" class="form-control" name="actor" value="{{ old('actor') }}" placeholder="Movie actor">
            </div>
            <div class="form-group ">
                <button type="submit" class="btn btn-primary">save</button>
            </div>

        </form>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" role="alert">
                    {{ $error }}
                </div>
            @endforeach
        @endif
        <br><br><br><br><br><br>
    </div>
@endsection
