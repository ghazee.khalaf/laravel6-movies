@extends('move.layout')
@section('content')
    <div class="container">
        <img src="/images/feature.jpg" alt="...">
        <div class="container">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Movie name</th>
                        <th scope="col">Movie category</th>
                        <th scope="col">Movie bio</th>
                        <th scope="col">Movie Image</th>
                        <th scope="col">Movie Rating</th>
                        <th scope="col">Movie Actor</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $i = 0;
                    @endphp

                    <tr>

                        <th scope="row">{{ ++$i }}</th>
                        <td>{{ $moveiss->name }}</td>
                        <td>{{ $category->name }}</td>
                        <td>{{ $moveiss->bio }}</td>
                        {{--  <td>{{ $moveiss->image_path }}</td>  --}}
                        <td><img src="/{{$moveiss->image_path}}" alt="Movie" style="width:350px;height:200px" ></td>
                        {{--  <td><img src="{{ url('storage/images/'.$moveiss->image_path }}" alt="Movie">{{$moveiss->image_path}}</td>  --}}
                        <td>{{ $moveiss->rating }}</td>
                        <td>{{ $moveiss->actor }}</td>

                    </tr>
                </tbody>
            </table>
        </div>
        <br><br><br><br><br><br><br><br>

    </div>
@endsection
