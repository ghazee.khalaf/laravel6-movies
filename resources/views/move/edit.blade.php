@extends('move.layout')
@section('content')
    <div class="container">
        <img src="/images/feature.jpg" alt="...">


        <form action="{{ route('moveis.update', $moveiss->id) }}" method="post" encrypted="multipart/form-data">
            @method('PUT')
            <!-- name -->
            <div class="form-group ">
                <label for="exampleFormControlInput1">Edit Movie name</label>
                <input type="text" value="{{ $moveiss->name }}" class="form-control" name="name" placeholder="Movie name">
            </div>
            <!-- category -->
            <div class="form-group">
                <label for="exampleFormControlInput1">Movie Catagory</label>
                <select class="form-control" name="category_id">
                    @foreach ($category as $cat)
                        <option value="{{ $cat->id }}" {{ $cat->id == old('category_id') ? 'selected' : '' }}>{{ $cat->name }}</option>
                    @endforeach
                </select>
            </div>

            {{-- <div class="form-group">
                <label for="exampleFormControlInput1">Edit Movie Catagory</label>
                <input type="text" class="form-control" value="{{ $moveiss->category }}" name="category"
                    placeholder="Movie Catagory">
            </div> --}}
            <!-- bio -->
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Edit Movie bio</label>
                <textarea class="form-control" name="bio" rows="2">{{ $moveiss->bio }}</textarea>
            </div>
            <!-- image -->
            <div class="form-group">
                <label for="exampleFormControlInput1">Edit Movie image</label>
                <input type="file" class="form-control" value="{{ $moveiss->image_path }}" name="image_path"
                    placeholder="Movie Catagory">
            </div>
            <!-- rating -->
            <div class="form-group">
                <label for="exampleFormControlInput1"> Edit Movie rating </label>
                <input type="number" class="form-control" value="{{ $moveiss->rating }}" name="rating" max="10" min="1">
            </div>
            <!-- actor -->
            <div class="form-group">
                <label for="exampleFormControlInput1">Edit Movie Actor</label>
                <input type="text" class="form-control" value="{{ $moveiss->actor }}" name="actor">
            </div>
            <!-- save -->
            <div class="form-group ">
                <button type="submit" class="btn btn-primary">save</button>
            </div>
            @csrf

        </form>
        <br><br><br><br><br><br>
    </div>
@endsection
