<form action="{{ route('moveis.store') }}" method="POST" encrypted="multipart/form-data">
    @csrf

    <div class="form-group ">
        <label for="exampleFormControlInput1">Movie Cover photo</label>
        <input type="file" class="form-control" name="file" accept="image/*">
    </div>



    <div class="form-group ">
        <button type="submit" class="btn btn-primary">save</button>
    </div>
</form>
