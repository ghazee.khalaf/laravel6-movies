@extends('move.layout')
@section('content')
    <div class="container">
        <img src="/images/feature.jpg" alt="...">


        <form action="{{ route('category.show', $category->id) }}" method="POST">
            @csrf
            <div class="form-group ">
                <label for="exampleFormControlInput1">Category name</label>
                <input type="text" class="form-control"  value={{ $category->name }} name="name" placeholder="Category name">
            </div>

            <div class="form-group ">
                <button type="submit" class="btn btn-primary">save</button>
            </div>


        </form>

    </div>
@endsection
