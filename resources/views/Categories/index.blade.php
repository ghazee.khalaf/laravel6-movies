@extends('move.layout')
@section('content')

    <div class="jumbotron container">
        <img src="/images/feature.jpg" alt="...">

        <br>
        <p class="lead">You can add category here... </p>

        <a class="btn btn-primary btn-lg" href="{{ route('moveis.create') }}" role="button">Add Movie</a>
        <a class="btn btn-primary btn-lg" href="{{ route('category.create') }}" role="button">Add Category</a>
    </div>
    <div class="container">
        @if ($message = Session::get('success'))
            <div class="alert alert-primary" role="alert">
                {{ $message }}
            </div>
        @endif
    </div>

    <div class="container">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">category name</th>
                    <th scope="col">Date created</th>
                    <th scope="col" style="width: 400px">Actions</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($category as $item)
                    <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->created_at->diffForHumans() }}</td>
                        <td>
                            <div class="row">
                                <div class="col-sm">
                                    <a class="btn btn-success" href="{{ route('category.edit', $item->id) }}"> Edit </a>
                                </div>
                                <div class="col-sm">
                                    <a class="btn btn-primary" href="{{ route('category.show', $item->id) }}"> Show </a>
                                </div>
                                <div class="col-sm">
                                    <form action="{{ route('category.destroy', $item->id) }}" method="post">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">delete </button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <br><br><br><br><br>
    </div>
@endsection
