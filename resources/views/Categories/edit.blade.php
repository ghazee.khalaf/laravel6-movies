@extends('move.layout')
@section('content')
    <div class="container">
        <img src="/images/feature.jpg" alt="...">


        <form action="{{ route('category.update', $category->id) }}" method="POST">
            @csrf
            @method('put')
            <div class="form-group ">
                <label for="exampleFormControlInput1">Category name</label>
                <input type="text"  value="{{ $category->name }}" class="form-control" name="name" placeholder="Category name">
            </div>

            <div class="form-group ">
                <button type="submit" class="btn btn-primary" >save</button>
            </div>


        </form>

    </div>
@endsection
