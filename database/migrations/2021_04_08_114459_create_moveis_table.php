<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoveisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moveis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('bio')->nullable();
            $table->macAddress('device')->nullable();
            $table->ipAddress('visitor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moveis');
    }
}
