<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRatitingPhotoActorsTableMoveis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('moveis', function (Blueprint $table) {
            $table->string('image_path');
            $table->integer('rating');
            $table->char('actor', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('moveis', function (Blueprint $table) {
            $table->dropColumn('actor');
            $table->dropColumn('rating');
            $table->dropColumn('image_path');
        });
    }
}
