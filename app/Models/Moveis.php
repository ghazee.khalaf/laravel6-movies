<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Moveis extends Model
{
    use HasFactory;
    protected $table = 'moveis';
    //  protected $fillable = ['name', 'category', 'bio', 'image_path', 'rating', 'actor'];
    protected $fillable = ['name', 'category_id', 'bio', 'rating', 'actor','image_path'];
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
