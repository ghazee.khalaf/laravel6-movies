<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Moveis;
use Illuminate\Http\Request;

class MoveisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $moveiss = Moveis::with('category')->get();
        return view('move.index', compact('moveiss'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('move.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5',
            'category_id' => 'required',
            'bio' => 'required|max:255',
            'rating' => 'required|numeric',
            'actor' => 'required|min:5',
            'image_path' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $path = $request->file('image_path')->store('public/cover');
        $path =explode('/', $path);
        array_shift($path);
        $path=implode('/', $path);
        // dd('storage/'.$path);
        $moviesData=$request->all();
        $moviesData['image_path']= 'storage/'.$path;
        // dd($path);
        // dd($moviesData);
        // dd($moviesData);
        $moveiss = Moveis::create($moviesData);
        return redirect()->route('moveis.index')->with('success', 'move added successfully');
    }

    /**
     * Display the specified resource.
     *
     */
    public function show($id)
    {
        //$category = Category::findorfail($id);

        $moveiss = Moveis::findorfail($id);
        $category = $moveiss->category;
        // dd($moveiss);
        return view('move.show', compact('moveiss', 'category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Moveis  $moveis
     * @return \Illuminate\Http\Response
     */
    public function edit($moveiss)
    {
        $moveiss = Moveis::findorfail($moveiss);
        $category = Category::all();
        return view('move.edit', [
            'moveiss' => $moveiss,
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Moveis  $moveis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $moveiss)
    {
        //   dd($request->all());
        $request->validate([
            'name' => 'required|min:5',
            'category_id' => 'required',
            'bio' => 'required|max:255',
            'rating' => 'required|numeric',
            'actor' => 'required|min:5',
            'image_path' => 'required'

        ]);
        //  dd($request->all());
        $moveiss = Moveis::findorfail($moveiss);
        //$moveiss = Moveis::update($request->all());
        $moveiss->update($request->all());
        return redirect()->route('moveis.index')->with('success', 'move updated successfully');
    }

    public function destroy($id)
    {
        Moveis::where('id', $id)->delete();
        return redirect()->route('moveis.index')->with('success', 'move deleted successfully');
    }
}
