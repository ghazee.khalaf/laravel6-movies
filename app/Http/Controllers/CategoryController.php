<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return view('Categories.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:5'
        ]);
        $category = Category::create($request->all());
        return redirect()->route('category.index')->with('success', 'category added successfully');
    }

    /**
     * Display the specified resource.
     *

     */
    public function show($id)
    {
        $category = Category::findorfail($id);
        return view('Categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $moveis
     * @return \Illuminate\Http\Response
     */
    public function edit($category)
    {
        $category = Category::findorfail($category);
        //dd($m);
        return view('Categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $moveis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $category)
    {
        $request->validate([
            'name' => 'required|min:5'
        ]);
        //  dd($request->all());
        $category = Category::findorfail($category);
        //$moveiss = Moveis::update($request->all());
        $category->update($request->all());
        return redirect()->route('category.index')->with('success', 'Category updated successfully');
    }



    public function destroy($id)
    {
        Category::where('id', $id)->delete();
        return redirect()->route('category.index')->with('success', 'category deleted successfully');
    }
}
