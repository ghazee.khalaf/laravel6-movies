<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('moveis', App\Http\Controllers\MoveisController::class);
Route::resource('category', App\Http\Controllers\CategoryController::class);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//route::get('category','App\Http\Controllers\CategoryController@index');
//route::get('add-category','App\Http\Controllers\CategoryController@create');
//route::put('/add-category','App\Http\Controllers\CategoryController@');

